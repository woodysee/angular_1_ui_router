//Node.js Package Manager module imports
const express = require("express");
const bodyParser = require("body-parser");
const Sequelize = require("sequelize");
const dotenv = require("dotenv");

//Environment variable declarations
dotenv.config();
const NODE_PORT = process.env.NODE_PORT || 3002;
const MYSQL_USER = process.env.MYSQL_USER;
const MYSQL_PASS = process.env.MYSQL_PASS;

//Server initialisation
const server = express();

server.use(bodyParser.urlencoded({ extended: false }));
server.use(bodyParser.json());

server.use(express.static(__dirname + "/../client/"));

const connection = new Sequelize("employees", MYSQL_USER, MYSQL_PASS, {
    host: "localhost",
    port: 3306,
    logging: console.log,
    dialect: "mysql",
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    }
});
const Employees = require("./models/employees")(connection, Sequelize);

const EMPLOYEES_API = "/api/employees";

/************//*
METHODS
*//************/

// CREATE employee
server.post(EMPLOYEES_API, (req, res) => {
    console.log("Adding one employee:" + JSON.stringify(req.body));
    const employee = req.body;
    // delete employee.emp_no;
    // const whereClause = {
    //     where: { emp_no: employee.emp_no },
    //     defaults: employee
    // };
    Employees.create(employee)
        .then(result => {
            console.log("200: Submitting data:");
            res.status(200).json(result);
    })
    .catch(error => {
        console.log(error);
        res.status(500).json(error);
    });
});

// UPDATE employee
server.put(EMPLOYEES_API, (req, res) => {
    console.log("Updating one employee: " + JSON.stringify(req.body));
    const emp_no = req.body.emp_no;
    console.log(emp_no);
    const whereClause = { limit: 1, where: { emp_no: emp_no } };
    Employees.findOne(whereClause)
        .then(result => {
            result.update("200: Updating data:", req.body);
            res.status(200).json(result);
        })
    .catch(error => {
        res.status(500).json(error);
    });
});

// GET all employees
server.get(EMPLOYEES_API, (req, res) => {
    console.log("Searching for: " + req.query.keyword);
    const keyword = req.query.keyword;
    const orderBy = req.query.orderBy;
    if (typeof orderBy == "null") {
        orderBy = "ASC";
    };
    console.log(`req.query.orderBy is ${req.query.orderBy}`);
    const itemsPerPage = parseInt(req.query.itemsPerPage);
    const currentPage = parseInt(req.query.currentPage);
    const offset = (currentPage - 1) * itemsPerPage;
    // console.log(keyword);
    // console.log(orderBy);
    // console.log(itemsPerPage);
    // console.log(currentPage);
    // console.log(typeof offset);
    // console.log(typeof keyword);
    const whereClause = {
        offset: offset,
        limit: itemsPerPage,
        where: {
            first_name: keyword
        },
        order: [
            ['first_name', orderBy],
            ["last_name", orderBy]
        ]
    };
    Employees.findAndCountAll(whereClause)
        .then(results => {
            res.status(200).json(results);
        })
        .catch(error => {
            res.status(500).json(error);
        })
    });

//GET 1 employee
server.get(EMPLOYEES_API + "/:emp_no", (req, res) => {
    console.log(req.params.emp_no);
    const emp_no = req.params.emp_no;
    console.log(emp_no);
    const whereClause = { limit: 1, where: { emp_no: emp_no } };
    Employees.findOne(whereClause)
        .then(result => {
            console.log("200: Getting 1 employee:", result);
            res.status(200).json(result);
        })
        .catch(error => {
            res.status(500).json(error);
        });
    });

//DELETE employee
server.delete(EMPLOYEES_API + "/:emp_no", (req, res) => {
    console.log(req.params.emp_no);
    const emp_no = req.params.emp_no;
    console.log(emp_no);
    const whereClause = { limit: 1, where: { emp_no: emp_no } };
    Employees.findOne(whereClause)
        .then(result => {
            result.destroy();
            res.status(200).json({});
        })
    .catch(error => {
        console.log("Deletion unsuccessful:", error);
        res.staus(410).json(error);
    });
});

/************//*
NODE.JS SERVER TO ANGULAR v1.x CLIENT REDIRECTS
(Place only at the bottom)
*//************/

server.get('/add', (req, res) => {
    console.log("Angular 1.x redirect from backend initiated:");
    res.redirect("/");
});

server.get('/about', (req, res) => {
    console.log("Angular v1.x redirect from backend initiated:");
    res.redirect("/");
});


//Mandatory server listener
server.listen(NODE_PORT, () => {
    console.log(
    "server/server.js: express server is now running locally on port: " +
        NODE_PORT
    );
});