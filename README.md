# Angular 1.x UI-Router #

For MacOS:

## Initialisation ##

1. Install backend packages.
```
npm install
```
2. Install frontend packages.
```
bower install
```
3. Touch a `/.env` file on the project root level and declare your node port, MySQL username and MySQL password there.
```
MYSQL_USER=
MYSQL_PASS=
NODE_PORT=
```

## Setting up the Employees Database ##
This full stack web app requires the use of MySQL Server (and MySQL Workbench).

1. Download & unzip `/test_db-master` from https://github.com/datacharmer/test_db.
2. From the unizpped test_db-master folder, look for `/employees.sql`.
3. In Terminal, change to the directory `/test_db-master`.
```
Your-Mac:somewhere You$ cd /path/to/test_db-master
Your-Mac:test_db-master You$
```
4. From `/test_db-master`, log in as root.
```
Your-Mac:test_db-master You$ mysql -uroot -p
```
5. In MySQL, create a new `employees` database.
```
CREATE DATABASE employees;
```
6. Import `/employees.sql` into `employees`.
```
USE employees;
SOURCE employees.sql;
```

## Running locally ##
1. Run the app on Terminal.
```
Your-Mac:angular_1_ui_router You$ nodemon
```
2. Open `localhost:3002` on your web browser.

## References ##
- NUS-ISS Stackup FSF
- [kenken77](https://bitbucket.org/kenken77/fsf17r4_day14)
- [Angular 1 UI-Router](https://ui-router.github.io/about/)