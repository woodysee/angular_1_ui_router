(()=>{
    angular.module("Client").config(Router);
    Router.$inject = ["$stateProvider", "$urlRouterProvider", "$locationProvider"];

    function Router($stateProvider,$urlRouterProvider,$locationProvider){
        $locationProvider.hashPrefix(""); /* by default '!' */
        $locationProvider.html5Mode(true); /*Needed for removal of hashbang in Client root URL (i.e. #!)*/
        $stateProvider
            .state('BasicSearch', {
                url: '/',
                templateUrl: "/views/search.basic.html",
                controller : 'RegVM',
                controllerAs : 'vm'
            })
            .state('AdvancedSearch', {
                url: '/',
                templateUrl: "/views/search.advanced.html",
                controller : 'RegVM',
                controllerAs : 'vm'
            })
            .state('About', {
                url: '/about',
                templateUrl: "/views/about.html"
            })
            .state('Add', {
                url: '/add',
                templateUrl: "/views/employee.add.html",
                controller : 'AddStaticRegVM',
                controllerAs : 'vm'
            });

            $urlRouterProvider.otherwise("/");
    };

})();