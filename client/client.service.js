(() => {

    angular.module("Client").service("RegSvc", ["$http", RegSvc]);

    function RegSvc($http) {
        const svc = this;
        //Query strng
        svc.searchEmployees = (keyword, orderBy, itemsPerPage, currentPage) => {
            return $http.get(`/api/employees?keyword=${keyword}&orderBy=${orderBy}&itemsPerPage=${itemsPerPage}&currentPage=${currentPage}`);
        };
        
        svc.getEmployee = (emp_no) => {
            return $http.get("/api/employees/" + emp_no);
        };

        svc.addEmployee = (employee) => {
            console.log(employee);
            return $http.post("/api/employees", employee);
        };

        svc.updateEmployee = (employee) => {
            console.log(employee);
            return $http.put("/api/employees", employee);
        };

        //Post by body over request
        svc.submitSearch = (value) => {
            const jsObject = {
                key: value
            };
            return $http.post("/api/employees", jsObject);
        };

        //Delete employee
        svc.deleteEmployee = (emp_no) => {
            console.log(emp_no);
            return $http.delete("/api/employees/"+ emp_no);
        };

    };

})();