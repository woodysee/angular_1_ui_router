(() => {
    angular
        .module("Client")
        .controller("RegVM", RegVM)
        .controller("AddRegVM", AddRegVM)
        .controller("AddStaticRegVM", AddStaticRegVM)
        .controller("EditRegVM", EditRegVM);

    RegVM.$inject = ["RegSvc", "$uibModal", "$document", "$scope", "$rootScope"];
    EditRegVM.$inject = ["$uibModalInstance", "RegSvc", "items", "$rootScope"];
    AddRegVM.$inject = ["$uibModalInstance", "RegSvc", "items", "$rootScope"];
    AddStaticRegVM.$inject = ["RegSvc", "$rootScope"];

    function AddRegVM($uibModalInstance, RegSvc, items, $rootScope) {
        const vm = this;

        // RegSvc.getEmployee(vm.edit).then((result)=>{
        //   console.log(result.data);
        //   vm.edit = result.data;
        //   console.log(vm.edit.birth_date);
        // });

        vm.closeModal = () => {
            $uibModalInstance.close(vm.run);
        };

        vm.autoFillDummy = () => {
            console.log("Dummy values used:");
            vm.add = {
                first_name: "Woody",
                last_name: "See",
                gender: "M",
                birth_date: "1990-11-10",
                hire_date: "2017-11-02",
                photourl: "https://i.imgur.com/EY3zw9R.png"
            };
        };

        vm.clearDummy = () => {
            console.log("Dummy values used:");
            vm.add = {
                first_name: "",
                last_name: "",
                gender: "",
                birth_date: "",
                hire_date: "",
                photourl: ""
            };
        };

        vm.addRecord = () => {
            console.log("Form submitted with valid data: vm.add = ");
            console.log(vm.add);
            console.log(vm.add.first_name);
            console.log(vm.add.last_name);
            console.log(vm.add.gender);
            console.log(vm.add.birth_date);
            console.log(vm.add.hire_date);
            console.log(vm.add.photourl);
            RegSvc.addEmployee(vm.add)
            .then(result => {
                console.log(result);
                $rootScope.$broadcast("refreshEmployeeList");
            })
            .catch(error => {
                console.log(error);
            });
            vm.closeModal();
        };
    }

    //Needed for the static route
    function AddStaticRegVM(RegSvc, $rootScope) {
        const vm = this;

        vm.autoFillDummy = () => {
            console.log("Dummy values used:");
            vm.add = {
            first_name: "Woody",
            last_name: "See",
            gender: "M",
            birth_date: "1990-11-10",
            hire_date: "2017-11-02",
            photourl: "https://i.imgur.com/EY3zw9R.png"
            };
        };

        vm.clearDummy = () => {
            console.log("Dummy values used:");
            vm.add = {
                first_name: "",
                last_name: "",
                gender: "",
                birth_date: "",
                hire_date: "",
                photourl: ""
            };
        };

        vm.addRecord = () => {
            console.log("Form submitted with valid data: vm.add = ");
            console.log(vm.add);
            console.log(vm.add.first_name);
            console.log(vm.add.last_name);
            console.log(vm.add.gender);
            console.log(vm.add.birth_date);
            console.log(vm.add.hire_date);
            console.log(vm.add.photourl);
            RegSvc.addEmployee(vm.add)
            .then(result => {
                console.log(result);
                $rootScope.$broadcast("refreshEmployeeList");
            })
            .catch(error => {
                console.log(error);
            });
        };
    };

    function EditRegVM($uibModalInstance, RegSvc, items, $rootScope) {
        const vm = this;
        // Selected employees
        vm.edit = items;
        console.log(vm.edit);

        // RegSvc.getEmployee(vm.edit).then((result)=>{
        //   console.log(result.data);
        //   vm.edit = result.data;
        //   console.log(vm.edit.birth_date);
        // });

        vm.closeModal = () => {
            $uibModalInstance.close(vm.run);
        };

        vm.updateRecord = () => {
            console.log("Form submitted with valid data:");
            console.log(vm.edit.first_name);
            console.log(vm.edit.last_name);
            console.log(vm.edit.gender);
            console.log(vm.edit.birth_date);
            console.log(vm.edit.hire_date);
            console.log(vm.edit.photourl);
            RegSvc.updateEmployee(vm.edit)
            .then(result => {
                console.log(result);
                $rootScope.$broadcast("refreshEmployeeList");
            })
            .catch(error => {
                console.log(error);
            });
            vm.closeModal();
        };
    }

    function RegVM(RegSvc, $uibModal, $document, $scope, $rootScope) {
        const vm = this;
        vm.format = "M/d/yy hh:mm:ss a"; //a is the timezone

        vm.raw = [];
        vm.employees = [];
        vm.searched = false; //Show search results count or no results found only after search btn is clicked
        vm.searchedSummary = ""; //Show a message summarising search results - count or no results found
        //Pagination initial values
        vm.orderBy = "asc"; //By default, it sorts the field searched in ascending order
        vm.pages = {
            currentPage: 1,
            maxSize: 5,
            totalItems: 0,
            itemsPerPage: 12,
            set: page => {
                vm.pages.currentPage = page;
            },
            changed: page => {
                console.log("Page changed to " + vm.pages.currentPage);
                searchEmployees(
                    vm.searchKeyword,
                    vm.orderBy,
                    vm.pages.itemsPerPage,
                    vm.pages.currentPage
                );
            }
        };

        $scope.$on("refreshEmployeeList", () => {
            console.log("Refreshing updated changes..." + vm.searchKeyword);
            RegSvc.searchEmployees(vm.searchKeyword, vm.orderBy, vm.pages.itemsPerPage, vm.pages.currentPage)
            .then(results => {
                vm.employees = results.data.rows;
            })
            .catch(error => {
                console.log(error);
            });
        });


        // CRUD Methods for Employees
        //Read
        vm.searchEmployees = () => {
            console.log("Searching employees...");
            RegSvc.searchEmployees(
                vm.searchKeyword,
                vm.orderBy,
                vm.pages.itemsPerPage,
                vm.pages.currentPage,
            ).then(results => {
                console.log(results.data);

                vm.employees = results.data.rows;

                // console.log(`results.data.length = ${results.data.count}`);
                vm.pages.totalItems = results.data.count;

                // console.log("vm.pages.itemsPerPage = ", vm.pages.itemsPerPage);
                // console.log("vm.pages.totalItems = ", vm.pages.totalItems);
                // console.log("vm.pages.itemsPerPage = ", vm.pages.itemsPerPage);

                //This statement can't be changed (i.e. $scope.numPages come from the Angular UI package)
                $scope.numPages = Math.ceil(
                vm.pages.totalItems / vm.pages.itemsPerPage
                );

                // console.log(`$scope.numPages = ${$scope.numPages}`);

                vm.searched = true;
                //Search summary after queried
                vm.searchedSummary =
                results.data.count == 0
                    ? `No results found for keyword: ${vm.searchKeyword}`
                    : `${results.data
                        .count} results found for keyword: ${vm.searchKeyword}`;
            })
            .catch(error => {
                console.log(error);
            });
        };

        //Create
        vm.addEmployee = (size, parentSelector) => {
            console.log("Adding employee...");
            let parentElem = parentSelector
            ? angular.element(
                $document[0].querySelector(".modal-demo " + parentSelector)
                )
            : undefined;
            let modalInstance = $uibModal
            .open({
                animation: vm.animationsEnabled,
                ariaLabelledBy: "modal-title",
                ariaDescribedBy: "modal-body",
                templateUrl: "/views/employee.add.html",
                controller: "AddRegVM",
                controllerAs: "vm",
                size: size,
                appendTo: parentElem,
                resolve: {
                items: function() {
                    return vm.items;
                }
                }
            })
            .result.catch(res => {
                //Kenneth's custom error
                if (
                ["cancel", "backdrop click", "escape key press"].indexOf(res) === -1
                )
                throw res;
            });
        };

        //Update
        vm.editEmployee = (employee, size, parentSelector) => {
            console.log("Editing employee: " + employee.emp_no);
            RegSvc.getEmployee(employee.emp_no)
            .then(result => {
                console.log("After then:");
                console.log(result.data);
                let parentElem = parentSelector
                ? angular.element(
                    $document[0].querySelector(".modal-demo " + parentSelector)
                    )
                : undefined;
                let modalInstance = $uibModal
                .open({
                    animation: vm.animationsEnabled,
                    ariaLabelledBy: "modal-title",
                    ariaDescribedBy: "modal-body",
                    templateUrl: "/views/employee.edit.html",
                    controller: "EditRegVM",
                    controllerAs: "vm",
                    size: size,
                    appendTo: parentElem,
                    resolve: {
                    items: function() {
                        return (vm.items = result.data);
                    }
                    }
                })
                .result.catch(res => {
                    //Kenneth's custom error
                    if (
                    ["cancel", "backdrop click", "escape key press"].indexOf(
                        res
                    ) === -1
                    )
                    throw res;
                });
            })
            .catch(error => {
                console.log(error);
            });
        };

        //Delete
        vm.deleteEmployee = (employee) => {
            console.log("Deleting an employee: " + employee.emp_no);
            RegSvc.deleteEmployee(employee.emp_no)
            .then(results => {
                $rootScope.$broadcast("refreshEmployeeList");
                console.log(
                "Successfully deleted: This should be empty - ",
                results.data.rows
                );
            })
            .catch(error => {
                console.log(error);
            });
        };
    }
})();
