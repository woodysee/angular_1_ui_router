(() => {
    
    angular.module('EMSApp').directive("myCurrentTime", myCurrentTime);

    function myCurrentTime(dateFilter){
        let format;
        return (scope, element, attrs) => {
            
            scope.$watch(attrs.myCurrentTime, (value) => {
                console.log(value);
                format = value;
                updateTime();
            });

            function updateTime() {
                const dt = dateFilter(new Date(), format);
                element.text(dt);
            };

            function updateLater() {
                setTimeout(function() {
                    updateTime(); // updates DOM
                    updateLater(); // schedules another update
                }, 1000);
            };

            updateLater();
        }
    }

}());